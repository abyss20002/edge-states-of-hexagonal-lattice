import numpy as np
from cmath import exp, pi, sqrt
import matplotlib.pylab as plt
from time import time


class KaneMele_edge():
    def __init__(self, M, t1, t2, r):
        s0 = np.array([[1, 0], [0, 1]])
        s1 = np.array([[0, 1], [1, 0]])
        s2 = np.array([[0, -1j], [1j, 0]])
        s3 = np.array([[1, 0], [0, -1]])
        z = np.zeros([2, 2])
        self.h00 = np.array([[M*s0, t1*s0+0.5j*r*(s1+sqrt(3)*s2), 1j*t2*s3, z],
                             [t1*s0-0.5j*r*(s1+sqrt(3)*s2), -M*s0,
                              t1*s0+1j*r*s1, 1j*t2*s3],
                             [-1j*t2*s3, t1*s0-1j*r*s1, M*s0,
                                 t1*s0+0.5j*r*(s1-sqrt(3)*s2)],
                             [z, -1j*t2*s3, t1*s0-0.5j*r*(s1-sqrt(3)*s2), -M*s0]]).transpose(0, 2, 1, 3).reshape(8, 8)
        self.h01 = np.array([[z, z, 1j*t2*s3, t1*s0-1j*r*s1],
                             [z, z, z, 1j*t2*s3],
                             [z, z, z, z],
                             [z, z, z, z]]).transpose(0, 2, 1, 3).reshape(8, 8)
        self.h10 = np.array([[-1j*t2*s3, z, z, z],
                             [t1*s0+0.5j*r*(-s1+sqrt(3)*s2),
                              1j*t2*s3, z, -1j*t2*s3],
                             [1j*t2*s3, z, -1j*t2*s3, t1 *
                                 s0+0.5j*r*(s1+sqrt(3)*s2)],
                             [z, z, z, 1j*t2*s3]]).transpose(0, 2, 1, 3).reshape(8, 8)
        self.h11 = np.array([[z, z, z, z],
                             [z, z, z, -1j*t2*s3],
                             [z, z, z, z],
                             [z, z, z, z]]).transpose(0, 2, 1, 3).reshape(8, 8)
        self.h12 = np.array([[z, z, z, z],
                             [z, z, z, z],
                             [1j*t2*s3, z, z, z],
                             [z, z, z, z]]).transpose(0, 2, 1, 3).reshape(8, 8)
        self.h02 = self.h01.transpose().conj()
        self.h20 = self.h10.transpose().conj()
        self.h21 = self.h12.transpose().conj()
        self.h22 = self.h11.transpose().conj()

    def Zigzag(self, kx, ny):
        I0 = np.diag([1]*ny)
        I1 = np.diag([1]*(ny-1), k=1)
        I2 = np.diag([1]*(ny-1), k=-1)

        H0 = np.kron(I0, self.h00)+np.kron(I1, self.h01) + \
            np.kron(I2, self.h02)
        H1 = (np.kron(I0, self.h10)+np.kron(I1, self.h11) +
              np.kron(I2, self.h12))*exp(1j*kx)
        H2 = H1.transpose().conj()
        H = H0+H1+H2
        return sorted(np.linalg.eigvalsh(H))

    def Armchair(self, ky, nx):
        I0 = np.diag([1]*nx)
        I1 = np.diag([1]*(nx-1), k=1)
        I2 = np.diag([1]*(nx-1), k=-1)

        H0 = np.kron(I0, self.h00)+np.kron(I1, self.h10) + \
            np.kron(I2, self.h20)
        H1 = (np.kron(I0, self.h01)+np.kron(I1, self.h11) +
              np.kron(I2, self.h21))*exp(1j*ky)
        H2 = H1.transpose().conj()
        H = H0+H1+H2
        return sorted(np.linalg.eigvalsh(H))

    def Plot_edge(self, N, n, klist, edge='zigzag'):
        dim = 8*n
        Ematrix = np.zeros([N, dim])
        start_time = time()
        if edge == "zigzag":
            for i in range(N):
                E = self.Zigzag(klist[i], n)
                Ematrix[i, :] = E[:]
        elif edge == "armchair":
            for i in range(N):
                E = self.Armchair(klist[i], n)
                Ematrix[i, :] = E[:]
        end_time = time()
        print(f'running time: {end_time-start_time} seconds')

        fig = plt.figure()
        ax = fig.add_subplot(111)
        for j in range(dim):
            ax.plot(klist, Ematrix[:, j], linewidth=0.5, color='tomato')
        ax.set_title('Kane-Mele '+edge+' Edge')
        ax.set_ylabel('E')
        if edge == 'zigzag':
            ax.set_xlabel("kx")
        elif edge == "armchair":
            ax.set_xlabel("ky")
        plt.show()


if __name__ == '__main__':
    N = 200
    n = 20
    edge = 'armchair'
    klist = np.linspace(-pi, pi, N)

    model = KaneMele_edge(M=-0.1, t1=-1, t2=-0.06, r=-0.05)
    model.Plot_edge(N, n, klist, edge)
