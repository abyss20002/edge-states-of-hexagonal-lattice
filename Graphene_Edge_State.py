import numpy as np
from cmath import exp, pi
import matplotlib.pylab as plt
from time import time


def H_Graphene_zigzag(t, kx, ny=3):
    A0 = np.diag([1]*ny)
    A1 = np.diag([1]*(ny-1), k=1)
    A2 = np.diag([1]*(ny-1), k=-1)

    h00 = np.array([[0, t, 0, 0], [t, 0, t, 0], [0, t, 0, t], [0, 0, t, 0]])
    h01 = np.zeros([4, 4])
    h01[0, 3] = t
    h02 = np.zeros([4, 4])
    h02[3, 0] = t
    h10 = np.zeros([4, 4])
    h10[1, 0] = t
    h10[2, 3] = t
    h11 = np.zeros([4, 4])
    h12 = np.zeros([4, 4])

    H0 = np.kron(A0, h00)+np.kron(A1, h01)+np.kron(A2, h02)
    H1 = (np.kron(A0, h10)+np.kron(A1, h11)+np.kron(A2, h12))*exp(1j*kx)
    H2 = H1.transpose().conj()
    return H0+H1+H2


def H_Graphene_armchair(t, ky, nx=3):
    A0 = np.diag([1]*nx)
    A1 = np.diag([1]*(nx-1), k=1)
    A2 = np.diag([1]*(nx-1), k=-1)

    h00 = np.array([[0, t, 0, 0], [t, 0, t, 0], [0, t, 0, t], [0, 0, t, 0]])
    h10 = np.zeros([4, 4])
    h10[1, 0] = t
    h10[2, 3] = t
    h20 = h10.transpose().conj()
    h01 = np.zeros([4, 4])
    h01[0, 3] = t
    h11 = np.zeros([4, 4])
    h21 = np.zeros([4, 4])

    H0 = np.kron(A0, h00)+np.kron(A1, h10)+np.kron(A2, h20)
    H1 = (np.kron(A0, h01)+np.kron(A1, h11)+np.kron(A2, h21))*exp(1j*ky)
    H2 = H1.transpose().conj()
    return H0+H1+H2


def H_Graphene_beared(t, kx, ny=3):

    A0 = np.diag([1]*ny)
    A1 = np.diag([1]*(ny-1), k=1)
    A2 = np.diag([1]*(ny-1), k=-1)

    h00 = np.array([[0, t, 0, 0], [t, 0, t, 0], [0, t, 0, t], [0, 0, t, 0]])
    h01 = np.zeros([4, 4])
    h01[0, 3] = t
    h02 = np.zeros([4, 4])
    h02[3, 0] = t
    h10 = np.zeros([4, 4])
    h10[1, 2] = t
    h11 = np.zeros([4, 4])
    h11[0, 3] = t
    h12 = np.zeros([4, 4])

    H0 = np.kron(A0, h00)+np.kron(A1, h01)+np.kron(A2, h02)
    H1 = (np.kron(A0, h10)+np.kron(A1, h11)+np.kron(A2, h12))*exp(1j*kx)
    H2 = H1.transpose().conj()
    return H0+H1+H2


def H_Graphene_twig(t, ky, nx=3):

    A0 = np.diag([1]*nx)
    A1 = np.diag([1]*(nx-1), k=1)
    A2 = np.diag([1]*(nx-1), k=-1)

    h00 = np.array([[0, t, 0, 0], [t, 0, t, 0], [0, t, 0, t], [0, 0, t, 0]])
    h10 = np.zeros([4, 4])
    h10[1, 0] = t
    h10[3, 2] = t
    h20 = h10.transpose().conj()
    h01 = np.zeros([4, 4])
    h11 = np.zeros([4, 4])
    h11[3, 0] = t
    h21 = np.zeros([4, 4])

    H0 = np.kron(A0, h00)+np.kron(A1, h10)+np.kron(A2, h20)
    H1 = (np.kron(A0, h01)+np.kron(A1, h11)+np.kron(A2, h21))*exp(1j*ky)
    H2 = H1.transpose().conj()
    return H0+H1+H2


def Eigval(H):
    return sorted(np.linalg.eigvalsh(H))


if __name__ == '__main__':
    N = 200
    n = 20
    t = 1
    edge = 'twig'
    klist = np.linspace(-pi, pi, N)

    start_time = time()
    dim = 4*n
    Ematrix = np.zeros([N, dim])
    if edge == 'zigzag':
        for i in range(N):
            E = Eigval(H_Graphene_zigzag(t, klist[i], n))
            Ematrix[i, :] = E[:]
    elif edge == 'armchair':
        for i in range(N):
            E = Eigval(H_Graphene_armchair(t, klist[i], n))
            Ematrix[i, :] = E[:]
    elif edge == 'beared':
        for i in range(N):
            E = Eigval(H_Graphene_beared(t, klist[i], n))
            Ematrix[i, :] = E[:]
    elif edge == 'twig':
        for i in range(N):
            E = Eigval(H_Graphene_twig(t, klist[i], n))
            Ematrix[i, :] = E[:]
    end_time = time()
    print(f'running time: {end_time-start_time} seconds')

    fig = plt.figure()
    ax = fig.add_subplot(111)
    for i in range(dim):
        ax.plot(klist, Ematrix[:, i], color="tomato", linewidth=0.5)
    ax.set_ylabel("E")
    ax.set_title("Graphene "+edge+" edge")
    if edge == 'zigzag' or edge == 'beared':
        ax.set_xlabel("kx")
    elif edge == "armchair" or edge == "twig":
        ax.set_xlabel("ky")
    plt.show()
