import numpy as np
from cmath import exp, pi
import matplotlib.pylab as plt
from time import time


class Haldane_edge():
    def __init__(self, M, t1, t2, phi):
        t2p = t2*exp(1j*phi)
        t2m = t2*exp(-1j*phi)
        self.h00 = np.array([[M, t1, t2m, 0],
                             [t1, -M, t1, t2m],
                             [t2p, t1, M, t1],
                             [0, t2p, t1, -M]])
        self.h01 = np.array([[0, 0, t2m, t1],
                             [0, 0, 0, t2m],
                             [0, 0, 0, 0],
                             [0, 0, 0, 0]])
        self.h10 = np.array([[t2p, 0, 0, 0],
                             [t1, t2m, 0, t2p],
                             [t2m, 0, t2p, t1],
                             [0, 0, 0, t2m]])
        self.h11 = np.array([[0, 0, 0, 0],
                             [0, 0, 0, t2p],
                             [0, 0, 0, 0],
                             [0, 0, 0, 0]])
        self.h12 = np.array([[0, 0, 0, 0],
                             [0, 0, 0, 0],
                             [t2m, 0, 0, 0],
                             [0, 0, 0, 0]])
        self.h02 = self.h01.transpose().conj()
        self.h20 = self.h10.transpose().conj()
        self.h21 = self.h12.transpose().conj()
        self.h22 = self.h11.transpose().conj()

    def Zigzag(self, kx, ny):
        I0 = np.diag([1]*ny)
        I1 = np.diag([1]*(ny-1), k=1)
        I2 = np.diag([1]*(ny-1), k=-1)

        H0 = np.kron(I0, self.h00)+np.kron(I1, self.h01) + \
            np.kron(I2, self.h02)
        H1 = (np.kron(I0, self.h10)+np.kron(I1, self.h11) +
              np.kron(I2, self.h12))*exp(1j*kx)
        H2 = H1.transpose().conj()
        H = H0+H1+H2
        return sorted(np.linalg.eigvalsh(H))

    def Armchair(self, ky, nx):
        I0 = np.diag([1]*nx)
        I1 = np.diag([1]*(nx-1), k=1)
        I2 = np.diag([1]*(nx-1), k=-1)

        H0 = np.kron(I0, self.h00)+np.kron(I1, self.h10) + \
            np.kron(I2, self.h20)
        H1 = (np.kron(I0, self.h01)+np.kron(I1, self.h11) +
              np.kron(I2, self.h21))*exp(1j*ky)
        H2 = H1.transpose().conj()
        H = H0+H1+H2
        return sorted(np.linalg.eigvalsh(H))

    def Plot_edge(self, N, n, klist, edge='zigzag'):
        dim = 4*n
        Ematrix = np.zeros([N, dim])
        start_time = time()
        if edge == "zigzag":
            for i in range(N):
                E = self.Zigzag(klist[i], n)
                Ematrix[i, :] = E[:]
        elif edge == "armchair":
            for i in range(N):
                E = self.Armchair(klist[i], n)
                Ematrix[i, :] = E[:]
        end_time = time()
        print(f'running time: {end_time-start_time} seconds')

        fig = plt.figure()
        ax = fig.add_subplot(111)
        for j in range(dim):
            ax.plot(klist, Ematrix[:, j], linewidth=0.5, color='tomato')
        ax.set_title('Haldane '+edge+' Edge')
        ax.set_ylabel('E')
        if edge == 'zigzag':
            ax.set_xlabel("kx")
        elif edge == "armchair":
            ax.set_xlabel("ky")
        plt.show()


if __name__ == '__main__':
    N = 200
    n = 20
    edge = 'zigzag'
    klist = np.linspace(-pi, pi, N)

    model = Haldane_edge(M=2/3, t1=1, t2=1/3, phi=pi/4)
    model.Plot_edge(N, n, klist, edge)
